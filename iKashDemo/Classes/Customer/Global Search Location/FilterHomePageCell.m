//
//  FilterHomePageCell.m
//  iKash
//
//  Created by indianic on 05/01/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import "FilterHomePageCell.h"

@implementation FilterHomePageCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
