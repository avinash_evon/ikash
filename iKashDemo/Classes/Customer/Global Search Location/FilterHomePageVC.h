//
//  FilterHomePageVC.h
//  iKash
//
//  Created by indianic on 05/01/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterHomePageVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>{

    IBOutlet UIButton *btnCity;
    IBOutlet UIButton *btnRadis;
    IBOutlet UISearchBar *searchBar;
    IBOutlet UITableView *tblCampaignFliter;
    
    NSIndexPath *indFilterPath;
    
    NSMutableDictionary *dicCampaignDetails;
    NSMutableArray *mutArrSelectedCampaignListAdded;
    
    NSMutableArray *mutArrSelectedCampaignCityListAdded,*mutArrSelectedCampaignRadiusListAdded;
    
    NSMutableArray *mutArrCampaignList,*mutArrCampaignCityList,*mutArrCampaignRadiusList;
    NSMutableArray *mutArrCampaignCitySearchList,*mutArrCampaignRadiusSearchList;
    NSString *strSelectedCampaign;
    BOOL isSearching;
    IBOutlet NSLayoutConstraint *topTableCampaign;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnApply;
@property (weak, nonatomic) IBOutlet UIButton *btnClearAll;

@property (nonatomic, strong) void(^popoverDismiss)();

@property(nonatomic,strong)NSString *strCatTitle, *strNavTitle;
@property(nonatomic,strong)NSString *strCatId;
- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnApplyAction:(id)sender;
- (IBAction)btnCityAction:(id)sender;
- (IBAction)btnRadiusAction:(id)sender;
- (IBAction)btnClearAllAction:(id)sender;

@end
