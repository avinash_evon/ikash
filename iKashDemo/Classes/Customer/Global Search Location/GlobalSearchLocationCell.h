//
//  GlobalSearchLocationCell.h
//  iKashDemo
//
//  Created by IndiaNIC on 05/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlobalSearchLocationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblSubTitle;

@end
