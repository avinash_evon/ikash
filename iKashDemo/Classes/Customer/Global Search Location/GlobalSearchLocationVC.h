//
//  GlobalSearchLocationVC.h
//  iKashDemo
//
//  Created by IndiaNIC on 05/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverAlertVC.h"
@interface GlobalSearchLocationVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIPopoverControllerDelegate>{
    PopOverAlertVC *objPopOverAlertVC;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tblViewGlobalSearchLocation;

@property (nonatomic, strong) UIPopoverController *notifyAndLocationPopover;
@property (nonatomic, strong) void(^popoverDismiss)();

- (IBAction)btnActionToGoBack:(id)sender;
- (IBAction)btnActionToClickSideMenu:(id)sender;
- (IBAction)btnActionToClickNotification:(id)sender;
@end
