//
//  CampaignDetailsVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"
#import "PopOverAlertVC.h"
#import "RateView.h"
#import "DJWStarRatingView.h"
@interface CampaignDetailsVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIPopoverControllerDelegate> {
    
    IBOutlet UIView *viewContent;
    IBOutlet NSLayoutConstraint *imgViewCampaignHeightConstraint;
    NSMutableArray *mutAryViewData;
    PopOverAlertVC *objPopOverAlertVC;
    RateView* objRateView;
    IBOutlet NSLayoutConstraint *scrllViewCampaignHeightConstraint;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;

@property (strong, nonatomic) IBOutlet UIButton *btnPopupView;
@property (nonatomic, strong) UIPopoverController *userVoucherPopover;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet DJWStarRatingView *starRatingView;
@property(nonatomic,assign)NSString *strCampaignId, *isFromFavourites;

@property (weak, nonatomic) IBOutlet UIImageView *imgCampaign;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;
@property (weak, nonatomic) IBOutlet UILabel *lblCampaignTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblViewsCount;
@property (weak, nonatomic) IBOutlet UILabel *lblLikesCount;
@property (weak, nonatomic) IBOutlet UILabel *lblDownloadCount;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewCount;
@property (weak, nonatomic) IBOutlet UILabel *lblColor;
@property (weak, nonatomic) IBOutlet UIButton *btnDownaloadVoucherClick;
@property (strong, nonatomic) IBOutlet UICollectionView *CollectionViewList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constImgHeight;

@property (weak, nonatomic) IBOutlet UIView *viewTopSeparator;
@property (weak, nonatomic) IBOutlet UIButton *btnReviews;


@property (strong, nonatomic) NSString *strCampaignStatus;
- (IBAction)btnActionToClickSupplierDetails:(UIButton *)sender;
- (IBAction)btnActionToClickReviewsDetails:(UIButton *)sender;
- (IBAction)btnFavouriteClick:(id)sender;
- (IBAction)btnActionToDownloadVoucherClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
@end
