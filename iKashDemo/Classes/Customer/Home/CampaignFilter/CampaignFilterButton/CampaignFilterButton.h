//
//  CampaignFilterButton.h
//  iKash
//
//  Created by indianic on 26/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CampaignFilterButton : UIButton
@property (strong, nonatomic) NSIndexPath *btnCampaignFilterButtonIndexPath;
@end
