//
//  CampaignCollectionCell.h
//  iKashDemo
//
//  Created by indianic on 02/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"
#import "DJWStarRatingView.h"

@interface CampaignCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgCampaign;
@property (weak, nonatomic) IBOutlet UIImageView *imgSurprice;
@property (weak, nonatomic) IBOutlet UIImageView *imgMaskImage;
@property (weak, nonatomic) IBOutlet DJWStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIImageView *imgCampaignBranch;
@property (weak, nonatomic) IBOutlet UILabel *lblColour;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constWidthImgViews;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constWidthImgDownloads;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constWidthImgLikes;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constHeightViewRating;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constTopLblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblViews;
@property (weak, nonatomic) IBOutlet UILabel *lblDownloads;
@property (weak, nonatomic) IBOutlet UILabel *lblLikes;

-(void)setDataWithDic :(NSDictionary*)dic;
@end
