//
//  CampaignCollectionCell.m
//  iKashDemo
//
//  Created by indianic on 02/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "CampaignCollectionCell.h"

@implementation CampaignCollectionCell


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self drowShadow];
        self.clipsToBounds = NO;
    }
    return self;
}

-(void)drowShadow{
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    self.layer.shadowRadius = 1;
}

-(void)setDataWithDic:(NSDictionary *)dic{
    self.viewBackground.clipsToBounds=NO;
    
   
    
    self.viewBackground.layer.cornerRadius = 0.0;//5.0;
    
    self.viewBackground.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.1f] CGColor];
    self.viewBackground.layer.shadowOffset = CGSizeMake(0, 1.0f);
    self.viewBackground.layer.shadowOpacity = 0.8f;
    self.viewBackground.layer.shadowRadius = 0.0;//0.5f;
    self.viewBackground.layer.masksToBounds = NO;//NO;
    
    UIBezierPath *maskPath;
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    NSArray *arrListFlipdata;
//    if(mutArrCampaignSurprice.count>0){
//        NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"c_campaign_id =[c] %@", [dic objectForKey:@"c_campaign_id"]];
//        arrListFlipdata = [mutArrCampaignSurprice filteredArrayUsingPredicate:aPredicate] ;
//        NSLog(@"%@",arrListFlipdata);
//    }
    
    if([[dic objectForKey:@"c_is_surprise"] isEqualToString:@"Yes"] && arrListFlipdata.count==0){
        
        self.imgSurprice.hidden=FALSE;
        self.lblColour.hidden=TRUE;
        self.starRatingView.hidden=TRUE;
        self.imgCampaignBranch.hidden=TRUE;
        self.lblTitle.hidden=TRUE;
        self.lblDescription.hidden=TRUE;
        self.imgMaskImage.hidden=TRUE;
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:self.imgSurprice.bounds
                                         byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                               cornerRadii:CGSizeMake(0.0, 0.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.imgSurprice.frame;
        maskLayer.path = maskPath.CGPath;
        self.imgSurprice.layer.mask = maskLayer;
        self.imgSurprice.backgroundColor=[UIColor clearColor];
        
        NSURL *urlProudctImage = [NSURL URLWithString:[dic objectForKey:@"surprise_image"]];
        
        [self.imgSurprice sd_setImageWithURL:urlProudctImage
                                              placeholderImage:nil
                                                       options:SDWebImageProgressiveDownload
                                                      progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                      }
                                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                         self.imgSurprice.clipsToBounds = YES;
                                                     }];
    }else if([[dic objectForKey:@"c_is_surprise"] isEqualToString:@"Yes"] && arrListFlipdata.count >0){
        
        self.imgSurprice.hidden=FALSE;
        self.lblColour.hidden=TRUE;
        self.starRatingView.hidden=TRUE;
        self.imgCampaignBranch.hidden=TRUE;
        self.lblTitle.hidden=TRUE;
        self.lblDescription.hidden=TRUE;
        self.imgMaskImage.hidden=TRUE;
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:self.imgSurprice.bounds
                                         byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                               cornerRadii:CGSizeMake(0.0, 0.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.imgSurprice.frame;
        maskLayer.path = maskPath.CGPath;
        self.imgSurprice.layer.mask = maskLayer;
        self.imgSurprice.backgroundColor=[UIColor clearColor];
        
        NSURL *urlProudctImage = [NSURL URLWithString:[dic objectForKey:@"camp_image_ios"]];
        
        [self.imgSurprice sd_setImageWithURL:urlProudctImage
                                              placeholderImage:nil
                                                       options:SDWebImageProgressiveDownload
                                                      progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                      }
                                                     completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                         self.imgSurprice.clipsToBounds = YES;
                                                     }];
    }else{
        self.imgSurprice.hidden=TRUE;
        self.lblColour.hidden=FALSE;
        self.starRatingView.hidden=FALSE;
        self.imgCampaignBranch.hidden=FALSE;
        self.lblTitle.hidden=FALSE;
        self.lblDescription.hidden=FALSE;
        self.imgMaskImage.hidden=FALSE;
        
        maskPath = [UIBezierPath bezierPathWithRoundedRect:self.imgCampaignBranch.bounds
                                         byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                               cornerRadii:CGSizeMake(5.0, 5.0)];
        maskLayer.frame = self.imgCampaignBranch.frame;
        maskLayer.path = maskPath.CGPath;
        self.imgCampaignBranch.layer.mask = maskLayer;
        
        NSString *strColor=[dic objectForKey:@"b_colour_code"];
        if(strColor.length>0)
        {
            strColor = [strColor substringFromIndex:1];
        }else{
            strColor=@"000000";
        }
        
        self.lblColour.backgroundColor=[appDelegate colorWithHexString:strColor];//
        
        bool isRatingAvailable = ![[dic objectForKey:@"averge_rating"] isEqualToString:@"Not Applicable"];
        
        if (isRatingAvailable) {
            
            double aintStar=[[dic objectForKey:@"averge_rating"] doubleValue];
            
           
            self.starRatingView.starSize=CGSizeMake(10, 10);//9,9
            self.starRatingView.numberOfStars=5;
            self.starRatingView.rating=aintStar;
          //  self.starRatingView.padding=10.0;//5
            
           // self.starRatingView.padding = 5;
            self.starRatingView.fillColor=[appDelegate colorWithHexString:@"F77739"];
            
            self.starRatingView.unfilledColor=[UIColor clearColor];
            self.starRatingView.strokeColor=[appDelegate colorWithHexString:@"F77739"];
            
         //   self.starRatingView.backgroundColor = [UIColor whiteColor];
            
            self.starRatingView.hidden = NO;
        }else{
            
            self.starRatingView.hidden = YES;
        }
        
        bool isLikesConuntAvailable = ![[dic objectForKey:@"cs_total_favourites"] isEqualToString:@"Not Applicable"];
        bool isViewsConuntAvailable = ![[dic objectForKey:@"cs_total_clicks"] isEqualToString:@"Not Applicable"];
        bool isDownloadsConuntAvailable = ![[dic objectForKey:@"cs_total_downloads"] isEqualToString:@"Not Applicable"];
        
        self.lblLikes.text = isLikesConuntAvailable ? [dic objectForKey:@"cs_total_favourites"] : @"";
        self.lblViews.text = isViewsConuntAvailable ? [dic objectForKey:@"cs_total_clicks"] : @"";
        self.lblDownloads.text = isDownloadsConuntAvailable ? [dic objectForKey:@"cs_total_downloads"] : @"";
        
        
        _constWidthImgViews.constant = isViewsConuntAvailable ? 14 : 0;
        _constWidthImgDownloads.constant = isDownloadsConuntAvailable ? 9 : 0;
        _constWidthImgLikes.constant = isLikesConuntAvailable ? 15 : 0;
        
        _constTopLblTitle.constant = isLikesConuntAvailable || isViewsConuntAvailable || isDownloadsConuntAvailable || isRatingAvailable ? IS_IPAD ? 146 : 120 : IS_IPAD ? 167 : 142;
        
        NSString *string = [dic objectForKey:@"c_title_"];
        //    if (string.length > 20) {
        //        string = [string substringToIndex:20];
        //    }
        self.lblTitle.text = [string uppercaseString];
        
        //NSLog(@"%@",string);
        //    self.lblTitle.text = [dic objectForKey:@"c_title_"];
        
        self.lblDescription.text = [dic objectForKey:@"c_short_description"];
        
        NSURL *urlProudctImage = [NSURL URLWithString:[dic objectForKey:@"camp_image_ios"]];
        
        [self.imgCampaignBranch sd_setImageWithURL:urlProudctImage
                                                    placeholderImage:nil
                                                             options:SDWebImageProgressiveDownload
                                                            progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                                            }
                                                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                               self.imgCampaignBranch.clipsToBounds = YES;
                                                           }];
        
        
    }
}

@end
