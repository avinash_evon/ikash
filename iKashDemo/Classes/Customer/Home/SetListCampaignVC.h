//
//  SetListCampaignVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverAlertVC.h"

@interface SetListCampaignVC : UIViewController<UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate>
{
    IBOutlet UIButton *btnNotification;
    IBOutlet UIButton *btnLocation;
    IBOutlet UIButton *btnSearch;
    IBOutlet UILabel *lblNavTitle;
    IBOutlet UILabel *lblNotificationCount;
    
    PopOverAlertVC *objPopOverAlertVC;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UIButton *btnNavTitle;

@property (nonatomic, strong) UIPopoverController *notifyAndLocationPopover;
@property (nonatomic, assign) BOOL boolIsShowingPopOverAlertVC, isDisAppear;
- (IBAction)btnActionToClickSideMenu:(id)sender;
- (IBAction)btnActionToClickNotification:(id)sender;
- (IBAction)btnActionToClickLocation:(id)sender;
@end
