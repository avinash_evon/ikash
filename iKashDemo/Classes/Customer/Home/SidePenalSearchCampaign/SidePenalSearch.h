//
//  SidePenalSearch.h
//  iKash
//
//  Created by indianic on 27/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidePenalSearch : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate, UIPopoverControllerDelegate>{
    IBOutlet UIButton *btnNotification;
    IBOutlet UIButton *btnLocation;

}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNoCampaign;

@property(nonatomic,assign)IBOutlet UICollectionView *collectionViewAll;
@property (strong, nonatomic) IBOutlet UIButton *btnPopupView;
@property (nonatomic, strong) UIPopoverController *campaignDetailsPopover;
@property(nonatomic,strong)NSMutableArray *mutAllCampaign;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
@end
