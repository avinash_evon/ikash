//
//  SupplierDetailsVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapView.h"


@interface SupplierDetailsVC : UIViewController
{
    IBOutlet NSLayoutConstraint *lblDescBottomConstraint;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;

@property (strong, nonatomic) IBOutlet UIImageView *imgViewSupplierLogo;
@property (strong, nonatomic) IBOutlet UILabel *lblSupplierName;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrllView;
@property (strong, nonatomic) IBOutlet UILabel *lblLocationAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNo;
@property (strong, nonatomic) IBOutlet UILabel *lblWebsite;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewMapImage;
@property (strong, nonatomic) IBOutlet UIButton *btnMap;
@property (strong, nonatomic) NSString *strSupplierId;
@property (strong, nonatomic) NSString *strCampaignId;

@property (strong, nonatomic) IBOutlet UIView *openLinkView;
- (IBAction)BtnActionToClickBack:(UIButton *)sender;
- (IBAction)btnActionToGoMapClick:(id)sender;
- (IBAction)btnActionToOpenLinkClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;

@property (nonatomic, strong) IBOutlet MapView *mapView;

@end
