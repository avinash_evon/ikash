//
//  SupplierMapVC.m
//  iKash
//
//  Created by indianic on 03/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MapView.h"
#import "Place.h"

@interface SupplierMapVC : UIViewController<CLLocationManagerDelegate>
{
    IBOutlet MapView *mapView;
    Place *userLoc;
    Place  *friendLoc;
    CLLocationManager  *locationManager;
    double sourceLat, sourceLong, destLat, destLong;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property(nonatomic,assign)double doubleLatitude;
@property(nonatomic,assign)double doubleLongitude;
@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;

@end
