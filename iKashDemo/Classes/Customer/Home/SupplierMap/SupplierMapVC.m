//
//  SupplierMapVC.m
//  iKash
//
//  Created by indianic on 03/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SupplierMapVC.h"
#import "TrailsMap.h"

@interface SupplierMapVC ()

@end

@implementation SupplierMapVC

#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (IS_IPHONE) {
        
        // Hide tabBar...
        //        self.tabBarController.tabBar.hidden = YES;
        self.lblNavTitle.text = [appDelegate getString:@"Supplier Route"];
//        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor],
//                                                                          NSFontAttributeName:[UIFont fontWithName:FontRobotoRegular size:17]}];
//        
//        self.navigationController.navigationBarHidden = NO;
//        self.navigationController.navigationBar.translucent = NO;
//        self.navigationController.navigationBar.barTintColor = kAppSupportedColorThemeColor;
//        
//        UIImage *leftbuttonImage = [UIImage imageNamed:@"ic-nav-back"];
//        UIButton *aLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [aLeftButton setImage:leftbuttonImage forState:UIControlStateNormal];
//        aLeftButton.frame = CGRectMake(0.0, 0.0, leftbuttonImage.size.width, leftbuttonImage.size.height);
//        UIBarButtonItem *aLeftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:aLeftButton];
//        [aLeftButton addTarget:self action:@selector(BtnActionToClickBack:) forControlEvents:UIControlEventTouchUpInside];
//        [self.navigationItem setLeftBarButtonItem:aLeftBarButtonItem];
    }
    // Do any additional setup after loading the view.
    
    // Current Lat/Long
//    sourceLat = 23.0300;[[AppDelegate sharedInstance]CurrentLat];
//    sourceLong =72.5800;[[AppDelegate sharedInstance]CurrentLong];
    
    sourceLat = [[AppDelegate sharedInstance]CurrentLat];
    sourceLong = [[AppDelegate sharedInstance]CurrentLong];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    mapView.mapView.settings.compassButton = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView.mapView.myLocationEnabled = YES;
    });
    if(!IS_IPAD)
    {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        
        [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self showMapRoute];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [mapView removeFromSuperview];
     mapView = nil;
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    return nil;
}

#pragma mark UIInterfaceOrientation Methods
- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:break;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [locationManager stopUpdatingLocation];
    locationManager = nil;
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)BtnActionToClickBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Custom Methods

-(void)showMapRoute
{
    userLoc = [[Place alloc] init];
    userLoc.name = @"user";
    userLoc.description = @"Current User Location";
    userLoc.latitude = sourceLat;
    userLoc.longitude = sourceLong;
    
    friendLoc = [[Place alloc] init];
    friendLoc.name = @"category";
    friendLoc.description = @"Destination Location";
  //  destLat = 22.3000;
    //destLong = 70.7833;
    
    destLat = _doubleLatitude;
    destLong = _doubleLongitude;

    friendLoc.latitude = destLat;
    friendLoc.longitude = destLong;
    
    [mapView.mapView setCamera:[GMSCameraPosition cameraWithLatitude:sourceLat longitude:sourceLong zoom:12.0]];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(loadMapview) userInfo:nil repeats:NO];
    
    locationManager = [[CLLocationManager alloc] init] ;
    if (isIOS8()) {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
    }
    [locationManager startUpdatingLocation];
    locationManager.delegate = self;
    locationManager.distanceFilter = 10.0; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters; // 100 m
}

#pragma mark - Location Manager Delegate

- (void)locationManager: (CLLocationManager *)manager didUpdateToLocation: (CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    userLoc = [[Place alloc] init];
    userLoc.name = @"user";
    userLoc.description = @"Current User Location";
    userLoc.latitude = newLocation.coordinate.latitude;
    userLoc.longitude = newLocation.coordinate.longitude;
    
    CGFloat latitude = newLocation.coordinate.latitude;
    CGFloat longitude = newLocation.coordinate.longitude;
    CLLocationCoordinate2D locationObj;
    locationObj.latitude = latitude;
    locationObj.longitude = longitude;
    
    friendLoc = [[Place alloc] init];
    friendLoc.name = @"category";
    friendLoc.description = @"Friend Location";
    friendLoc.latitude = 37.7833;
    friendLoc.longitude = 122.4167;

    [mapView showRouteFromSourceLatitude:userLoc.latitude SourceLongitude:userLoc.longitude DestinationLatitude:destLat DestinationLongitude:destLong];
    
    
//    [mapView showRouteFromSourceLatitude:23.0300 SourceLongitude:72.5800 DestinationLatitude:22.3000 DestinationLongitude:70.7833];
}

-(BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager
{
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSMutableString *errorString = [[NSMutableString alloc] init];
    if ([error domain] == kCLErrorDomain) {
        switch ([error code]) {
            case kCLErrorDenied:
                [errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationDenied", nil)];
                //showAlertView(appName,locationServiceOn);
                break;
            case kCLErrorLocationUnknown:
                [errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
                //showAlertView(appName,locationServiceOn);
                break;
            default:
                [errorString appendFormat:@"%@ %ld\n", NSLocalizedString(@"GenericLocationError", nil), (long)[error code]];
                break;
        }
    } else {
        [errorString appendFormat:@"Error domain: \"%@\"  Error code: %ld\n", [error domain], (long)[error code]];
        [errorString appendFormat:@"Description: \"%@\"\n", [error localizedDescription]];
    }
}

-(BOOL)checkForLocationPrivacy
{
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        return TRUE;
    }
    return FALSE;
}


#pragma mark - Other Methods

-(void)loadMapview
{
    
    userLoc = [[Place alloc] init];
    userLoc.name = @"user";
    userLoc.description = @"Current User Location";
    userLoc.latitude = sourceLat;
    userLoc.longitude = sourceLong;
    NSLog(@"%f",sourceLat);
    NSLog(@"%f",sourceLong);
    NSLog(@"%f",destLat);
    NSLog(@"%f",destLong);
    
   // [mapView showRouteFromSourceLatitude:userLoc.latitude SourceLongitude:userLoc.longitude DestinationLatitude:destLat DestinationLongitude:destLong];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnGetDirectionsAction : (UIButton *) btn{
    NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f",sourceLat, sourceLong, self.doubleLatitude, self.doubleLongitude];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: directionsURL]];
}

@end

