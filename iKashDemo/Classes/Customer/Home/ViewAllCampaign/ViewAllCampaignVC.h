//
//  ViewAllCampaignVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"
#import "PopOverAlertVC.h"

@interface ViewAllCampaignVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UIPopoverControllerDelegate>{
    IBOutlet UIButton *btnNotification;
    IBOutlet UIButton *btnLocation;
    IBOutlet UILabel *lblNavTitle;
    PopOverAlertVC *objPopOverAlertVC;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UIButton *btnPopupView;
@property (weak, nonatomic) IBOutlet UILabel *lblNoCampaign;
@property (nonatomic, strong) UIPopoverController *notifyAndLocationPopover, *campaignDetailsPopover, *campaignFilterPopover;
@property(nonatomic,assign)IBOutlet UICollectionView *collectionViewAll;
@property(nonatomic,strong)NSMutableArray *mutAllCampaign;
@property(nonatomic,assign)NSString *strTitle, *strBranchID;
@property(nonatomic,assign)NSString *strCategoryId;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
@end
