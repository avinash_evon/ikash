//
//  LeftSideMenuVC.h
//  Vanished
//
//  Created by IndiaNIC on 10/09/15.
//  Copyright (c) 2015 IndiaNIC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftSideMenuVC : UIViewController<UITableViewDelegate,UITableViewDataSource>{
//    IBOutlet UITableView *tblViewSideMenu;
    NSMutableArray *mutImageArray, *mutDataArray;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblTitleExplore;
    IBOutlet UIButton *btnClearall;

}

@property (strong, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property(nonatomic,weak) IBOutlet UITableView * tblViewSideMenu;
@property(nonatomic,strong) NSMutableArray * dataModelArray;

@property (nonatomic,strong) IBOutlet UITableView *tblForCollapse;
@property (nonatomic, retain) NSArray *arrayOriginal;
@property (nonatomic, retain) NSMutableArray *arForTable;

-(void)miniMizeThisRows:(NSArray*)ar;
-(IBAction)ClearAllSelection:(id)sender;
@end
