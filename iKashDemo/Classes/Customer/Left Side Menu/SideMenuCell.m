//
//  SideMenuCellTableViewCell.m
//  Vanished
//
//  Created by IndiaNIC on 11/09/15.
//  Copyright (c) 2015 IndiaNIC. All rights reserved.
//

#import "SideMenuCell.h"

@implementation SideMenuCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
