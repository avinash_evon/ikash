//
//  MoreVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreVC : UIViewController<UIPopoverControllerDelegate>
{
   
}

@property (assign) BOOL puchFlag;

@property (weak, nonatomic) IBOutlet UIView *backView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (weak, nonatomic) IBOutlet UIButton *switchPushNotify;



@property (strong, nonatomic) IBOutlet UIScrollView *scrllView;
@property (strong, nonatomic) IBOutlet UIView *alertView;
@property (strong, nonatomic) IBOutlet UIView *alertInnerView;
@property (strong, nonatomic) IBOutlet UILabel *lblSelectLanguage;
@property (strong, nonatomic) IBOutlet UIButton *btnRadioEnglish;
@property (strong, nonatomic) IBOutlet UILabel *lblEnglishText;
@property (strong, nonatomic) IBOutlet UIButton *btnRadioDutch;
@property (strong, nonatomic) IBOutlet UILabel *lblDutchText;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;

@property (strong, nonatomic) IBOutlet UILabel *btnNavTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnNotifications;
@property (strong, nonatomic) IBOutlet UILabel *lblLanguageText;
@property (strong, nonatomic) IBOutlet UIButton *btnLanguage;
@property (strong, nonatomic) IBOutlet UIButton *btnPushNotification;
@property (strong, nonatomic) IBOutlet UISwitch *switchPush;
@property (strong, nonatomic) IBOutlet UIButton *btnLogout;

@property (strong, nonatomic) IBOutlet UILabel *lblAppVersion;

@property (nonatomic, strong) UIPopoverController *notifyAndLocationPopover;


- (IBAction)BtnPushNotifyAction:(UIButton *)sender;




- (IBAction)btnActionToSideMenuClick:(id)sender;
- (IBAction)btnActionToNotificationsClick:(id)sender;
- (IBAction)btnActionToLanguageClick:(id)sender;
- (IBAction)btnActionToLogoutClick:(id)sender;
- (IBAction)switchOnOff:(id)sender;

- (IBAction)btnActionToPopCancel:(id)sender;
- (IBAction)btnActionToPopSubmit:(id)sender;
- (IBAction)btnActionToRemovePopView:(id)sender;
- (IBAction)btnSelectEnglish:(id)sender;
- (IBAction)btnSelectDutch:(id)sender;

@end
