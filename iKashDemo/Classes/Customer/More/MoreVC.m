//
//  MoreVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "MoreVC.h"
#import "NotificationVC.h"
#import "LoginVC.h"
#import "SidePenalSearch.h"

@interface MoreVC (){
    NSString *strPush;
    
    UIView *demoView;
    
     BOOL puchFlag;
}

@end

@implementation MoreVC

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _backView.hidden = TRUE;
    _puchFlag = TRUE;
    
    [_switchPushNotify setImage:[UIImage imageNamed:@"btn-switch-selected"] forState:UIControlStateNormal];
    
   //  [_switchPushNotify setBackgroundImage:[UIImage imageNamed:@"btn-switch-selected"] forState:UIControlStateNormal];
    
   NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    #if DEV_SERVER
    version = [version stringByAppendingFormat:@" DEV"];
    #else
    version = [version stringByAppendingFormat:@" TEST"];
    #endif
    _lblAppVersion.text = [NSString stringWithFormat:@"App version: %@",version];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUpHeaderView) name:@"MFSideMenuStateClosed" object:nil];
    self.navigationController.navigationBarHidden = YES;
    
    [self SetUpPage];
    
    if(!IS_IPAD){
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        [self adjustViewsForOrientationOnLoad:orientation];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:nil];
    }
}

-(void)setUpHeaderView{
    
    if([[AppDelegate sharedInstance].strTabSelectedIndex isEqualToString:@"3"]){
        if([[AppDelegate sharedInstance].mutArrSidePenalCategoryList count]>0){
            UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            SidePenalSearch *objSidePenalSearch = [aStoryboard instantiateViewControllerWithIdentifier:@"SidePenalSearch"];
            [self.navigationController pushViewController:objSidePenalSearch animated:YES];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MFSideMenuStateClosed" object:nil];
        }
    }
}

#pragma mark UIInterfaceOrientation Methods

- (void)orientationChanged:(NSNotification *)notification{
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientationOnLoad:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    switch (orientation){
        case UIInterfaceOrientationPortrait:
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            self.ContraintHeight.constant=64;
            self.leftContraintBottom.constant=0;
            self.titleContraintBottom.constant=0;
            self.rightContraintBottom.constant=0;
            [[UIApplication sharedApplication] setStatusBarHidden: NO];
            //load the portrait view
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
            
        case UIInterfaceOrientationLandscapeRight:{
            self.ContraintHeight.constant=38;
            self.leftContraintBottom.constant=-3;
            self.titleContraintBottom.constant=-3;
            self.rightContraintBottom.constant=-3;
            [[UIApplication sharedApplication] setStatusBarHidden: YES];
            //load the landscape view
        }
            break;
        case UIInterfaceOrientationUnknown:
            break;
    }
}

#pragma mark- UIButton Click Action Methods

- (IBAction)BtnPushNotifyAction:(UIButton *)sender {
    
    if (_puchFlag == TRUE){
      
        _puchFlag = FALSE;
        
          [_switchPushNotify setImage:[UIImage imageNamed:@"btn-switch-deselected"] forState:UIControlStateNormal];
        
        //  [_switchPushNotify setBackgroundImage:[UIImage imageNamed:@"btn-switch-deselected"] forState:UIControlStateNormal];
        
    }
    else{
         _puchFlag = TRUE;
        
      [_switchPushNotify setImage:[UIImage imageNamed:@"btn-switch-selected"] forState:UIControlStateNormal];
    }
    
    
    
}

- (IBAction)btnActionToSideMenuClick:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        
    }];
}

- (IBAction)btnActionToNotificationsClick:(id)sender {
    if (IS_IPAD) {
        UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        NotificationVC *objNotificationVC = [aStoryboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
        self.notifyAndLocationPopover = [[UIPopoverController alloc] initWithContentViewController:objNotificationVC];
        self.notifyAndLocationPopover.delegate = self;
        self.notifyAndLocationPopover.popoverContentSize = CGSizeMake(320.0, 600.0);
        self.view.alpha = 0.6;
        [self.notifyAndLocationPopover presentPopoverFromRect:[(UIButton *)sender bounds]
                                                       inView:self.btnNotifications
                                     permittedArrowDirections:UIPopoverArrowDirectionUp
                                                     animated:YES];
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
        NotificationVC *objNotificationVC = [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
        [self.navigationController pushViewController:objNotificationVC animated:YES];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
    self.notifyAndLocationPopover.delegate = nil;
    return YES;
}

- (IBAction)btnActionToLanguageClick:(id)sender {
    [self.alertInnerView.layer setCornerRadius:3.0f];
    [self.alertInnerView.layer setMasksToBounds:YES];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"Language"] isEqualToString:@"en"]) {
        self.btnRadioEnglish.selected = YES;
        self.btnRadioDutch.selected = NO;
    }else{
        self.btnRadioEnglish.selected = NO;
        self.btnRadioDutch.selected = YES;
    }
    self.alertView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.alertView.frame = self.view.frame;
    [self.view addSubview:self.alertView];
}

- (IBAction)btnActionToLogoutClick:(id)sender {
    
  /*  UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:APPTITLE message:[appDelegate getString:@"ALERTLOGOUT"] delegate:self cancelButtonTitle:[appDelegate getString:@"No"] otherButtonTitles:[appDelegate getString:@"Yes"], nil];
    alertView.tag=200;
    [alertView show];*/
    
    _backView.hidden = FALSE;
    
    [self logoutAlertView];
}

- (IBAction)switchOnOff:(id)sender {
    if([sender isOn]){
        strPush=@"1";
        NSLog(@"Switch is ON");
    } else{
        strPush =@"0";
        NSLog(@"Switch is OFF");
    }
    [self PushUpdateWS];
}

- (IBAction)btnActionToPopCancel:(id)sender {
    [self.alertView removeFromSuperview];
}

- (IBAction)btnActionToPopSubmit:(id)sender {
    if (self.btnRadioEnglish.selected == YES) {
        //  self.lblLanguageText.text = @"English";
        [USERDEFAULTS setValue:@"en" forKey:@"Language"];
    }else{
        //self.lblLanguageText.text = @"Dutch";
        [USERDEFAULTS setValue:@"nl" forKey:@"Language"];
    }
    [USERDEFAULTS synchronize];
    NSDictionary *aDictLanguageData = [USERDEFAULTS objectForKey:@"Language"];
    NSLog(@"aDictLanguageData = %@", aDictLanguageData);
    
    [self SetUpPage];
    [self.alertView removeFromSuperview];
}

- (IBAction)btnActionToRemovePopView:(id)sender {
    [self.alertView removeFromSuperview];
}

- (IBAction)btnSelectEnglish:(id)sender {
    self.btnRadioEnglish.selected = YES;
    self.btnRadioDutch.selected = NO;
}

- (IBAction)btnSelectDutch:(id)sender {
    self.btnRadioEnglish.selected = NO;
    self.btnRadioDutch.selected = YES;
}

#pragma mark - AlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag==200){
        if(buttonIndex==1){
            [appDelegate RemoveUserDefaults];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
            __block UINavigationController *aNv = nil;
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserLoginDetails"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            if ([[AppDelegate sharedInstance].window.rootViewController isKindOfClass:[MFSideMenuContainerViewController class]]) {
                LoginVC *objLoginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                aNv = [[UINavigationController alloc] initWithRootViewController:objLoginVC];
                [AppDelegate sharedInstance].window.rootViewController = aNv;
            } else {
                [self.navigationController popToRootViewControllerAnimated:YES];
                [AppDelegate sharedInstance].window.rootViewController = self.navigationController;
            }
        }
    }
}

#pragma mark - Webservice Methods

- (void)PushUpdateWS{
    if([self NointerNetCheking]){
        NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                      strPush,@"is_push",
                                      [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                      nil];
        [[Webservice sharedInstance] callWebserviceWithMethodName:customer_UpdatePushWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
            if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
                //[appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"]];
            }
        } withFailureBlock:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }else{
        if(_switchPush.isOn){
            [_switchPush setOn:NO];
            strPush=@"0";
            NSLog(@"Switch is ON");
        } else{
            [_switchPush setOn:YES];
            strPush =@"1";
            NSLog(@"Switch is OFF");
        }
    }
}

#pragma mark SetUpPage

-(void)SetUpPage{
    
    [_btnNotifications setTitle:[appDelegate getString:@"Notifications"] forState:UIControlStateNormal];
    [_btnLanguage setTitle:[appDelegate getString:@"Language"] forState:UIControlStateNormal];
    [_btnPushNotification setTitle:[appDelegate getString:@"Push Notification"] forState:UIControlStateNormal];
    [_btnLogout setTitle:[appDelegate getString:@"Logout"] forState:UIControlStateNormal];
    [_btnNavTitle setText:[appDelegate getString:@"More"]];
    [_lblEnglishText setText:[appDelegate getString:@"English"]];
    [_lblDutchText setText:[appDelegate getString:@"Dutch"]];
    [_lblSelectLanguage setText:[appDelegate getString:@"Select Language"]];
    [_btnCancel setTitle:[appDelegate getString:@"Cancel"] forState:UIControlStateNormal];
    [_btnSubmit setTitle:[appDelegate getString:@"Submit"] forState:UIControlStateNormal];
    
    if([[USERDEFAULTS objectForKey:@"Language"] isEqualToString:@"en"]){
        self.lblLanguageText.text = @"English";
    }else{
        self.lblLanguageText.text = @"Dutch";
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ForceFullyReloadDataWhenLanguageChange" object:nil];
}

-(void)viewDidLayoutSubviews{
    if (self.view.frame.size.height == 320) {
        UIInterfaceOrientation orientation = UIApplication.sharedApplication.statusBarOrientation;
        if(UIInterfaceOrientationIsPortrait(orientation)){
            _scrllView.scrollEnabled = NO;
        }else{
            _scrllView.scrollEnabled = YES;
        }
    }
}

-(BOOL)NointerNetCheking{
    
    if(![[AppDelegate sharedInstance] checkNetworkStatus]){
        [appDelegate showAlertView:[appDelegate getString:@"INTERNET_CONNECTION_ERROR"] Tag:@""];
        return NO;
    }
    return YES;
}


- (void)logoutAlertView
{
   
    demoView = [[UIView alloc] initWithFrame:CGRectMake(36, 354, 341, 192)];
    
    demoView.backgroundColor = [UIColor whiteColor];
    
    demoView.layer.cornerRadius = 10;
     
    demoView.layer.masksToBounds = true;

    UILabel *textLbl = [[UILabel alloc] initWithFrame:CGRectMake(57, 64, 300, 21)];//255
  
   
     UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(22, 112, 138, 40)];
    
    [btn1 setTitle:@"No" forState:UIControlStateNormal];
    
    
   
    
     UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(177, 112, 138, 40)];
    
     [btn2 setTitle:@"Yes" forState:UIControlStateNormal];
    
    [btn2 setTitleColor:[UIColor colorWithRed:249/255.0 green:119/255.0 blue:45/255.0 alpha:1.0] forState:UIControlStateNormal];
    
    btn2.layer.borderWidth = 2.0f;
  
    btn2.layer.borderColor = [UIColor colorWithRed:249.0/255.0f green:189.0/255.0f blue:119.0/255.0f alpha:45.0].CGColor;
    
    btn2.backgroundColor = [UIColor whiteColor];
    
     UIButton *btn3 = [[UIButton alloc] initWithFrame:CGRectMake(289, 10, 45, 45)];
    
    
    
    btn1.backgroundColor =  [UIColor colorWithRed:249/255.0 green:119/255.0 blue:45/255.0 alpha:1.0];
    
    
    
     btn3.backgroundColor = [UIColor clearColor];
    
  [btn3 setImage:[UIImage imageNamed:@"ic-nav-delete"] forState:UIControlStateNormal];
    
    [btn1 addTarget:self action:@selector(noBtnSelected) forControlEvents:UIControlEventTouchUpInside];
    
     [btn2 addTarget:self action:@selector(yesBtnSelected) forControlEvents:UIControlEventTouchUpInside];
    
     [btn3 addTarget:self action:@selector(noBtnSelected) forControlEvents:UIControlEventTouchUpInside];
    
    textLbl.text = @"Do You really want to Logout?";
    
    textLbl.backgroundColor = [UIColor clearColor];
    
      [demoView addSubview:textLbl];
    
     [demoView addSubview:btn1];
     [demoView addSubview:btn2];
     [demoView addSubview:btn3];
   
    
    [self.view addSubview:demoView];

   // return demoView;
}

-(void)noBtnSelected
{
    demoView.hidden = TRUE;
    
    _backView.hidden = TRUE;

}

-(void)yesBtnSelected
{
   // demoView.hidden = TRUE;
    [appDelegate RemoveUserDefaults];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    __block UINavigationController *aNv = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserLoginDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[AppDelegate sharedInstance].window.rootViewController isKindOfClass:[MFSideMenuContainerViewController class]]) {
        LoginVC *objLoginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        aNv = [[UINavigationController alloc] initWithRootViewController:objLoginVC];
        [AppDelegate sharedInstance].window.rootViewController = aNv;

}
    
}

@end
