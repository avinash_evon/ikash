//
//  FavouriteCampaignVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"
@interface FavouriteCampaignVC : UIViewController<UIPopoverControllerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblNoRecordFound;
@property (strong, nonatomic) IBOutlet UICollectionView *favouriteClnView;
@property (nonatomic, strong) UIPopoverController *campaignDetailsPopover;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
@end
