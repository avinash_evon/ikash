//
//  NewsLetterEmailVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsLetterEmailVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnNewletter;
@property (weak, nonatomic) IBOutlet UIButton *btnCampaign;
- (IBAction)btnNewletterClick:(id)sender;
- (IBAction)btnCampaignClick:(id)sender;



@end
