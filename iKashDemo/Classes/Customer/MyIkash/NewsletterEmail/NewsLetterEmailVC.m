//
//  NewsLetterEmailVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "NewsLetterEmailVC.h"

@interface NewsLetterEmailVC (){
    NSString *strNewletter;
    NSString *strDailyCampaign;
}

@end

@implementation NewsLetterEmailVC

- (IBAction)NewsletterBtnClk:(UIButton *)sender {
    
    
    [self.navigationController popViewControllerAnimated:TRUE];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateNewsletterSettingClick) name:@"EditNewsData" object:nil];
    [self GetUserNewsletterClick];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnNewletterClick:(id)sender {
    if(_btnNewletter.selected){
        _btnNewletter.selected=FALSE;
        strNewletter=@"0";
    }else{
        _btnNewletter.selected=TRUE;
        strNewletter=@"1";
    }
}

- (IBAction)btnCampaignClick:(id)sender {
    if(_btnCampaign.selected){
        _btnCampaign.selected=FALSE;
        strDailyCampaign=@"0";
    }else{
        _btnCampaign.selected=TRUE;
        strDailyCampaign=@"1";
    }
}

//newsletter_n_dailiy_mail_thing
-(void)UpdateNewsletterSettingClick{
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strNewletter,@"is_newsletter",
                                  strDailyCampaign,@"is_daily",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)GetUserNewsletterClick{
    NSLog(@"%@",[USERDEFAULTS objectForKey:@"Language"]);
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    NSLog(@"%@",aDict);
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetPersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            if([[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"is_daily_emails"] isEqualToString:@"1"]){
                _btnCampaign.selected=TRUE;
                strDailyCampaign=@"1";
            }else{
                _btnCampaign.selected=FALSE;
                strDailyCampaign=@"0";
            }
            if([[[[responseData objectForKey:@"data"] objectAtIndex:0] objectForKey:@"is_newsletter"] isEqualToString:@"1"]){
                _btnNewletter.selected=TRUE;
                strNewletter=@"1";
            }else{
                _btnNewletter.selected=FALSE;
                strNewletter=@"0";
            }
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
