//
//  SettingContainer.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AppDelegate.h"

@interface SettingContainer : UIViewController<UIPopoverControllerDelegate>{
    BOOL isSettingsClick;
     IBOutlet UIImageView *imgPointerNotication;
    
    
   // AppDelegate *mainDelegate;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;


@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
@property (weak, nonatomic) IBOutlet UIButton *btnNewsletter;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;
@property (weak, nonatomic) IBOutlet UIButton *btnReview;
@property (weak, nonatomic) IBOutlet UIButton *btnVoucher;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgSettings;
@property (weak, nonatomic) IBOutlet UILabel *lblSettings;
@property (weak, nonatomic) IBOutlet UIImageView *imgFavourite;
@property (weak, nonatomic) IBOutlet UILabel *lblFavourite;
@property (weak, nonatomic) IBOutlet UIImageView *imgNewsletter;
@property (weak, nonatomic) IBOutlet UILabel *lblNewsletter;
@property (weak, nonatomic) IBOutlet UIImageView *imgReview;
@property (weak, nonatomic) IBOutlet UILabel *lblReview;
@property (weak, nonatomic) IBOutlet UIImageView *imgVoucher;
@property (weak, nonatomic) IBOutlet UILabel *lblVoucher;
@property (weak, nonatomic) IBOutlet UIView *viewSettings;
@property (weak, nonatomic) IBOutlet UIView *viewReview;
@property (weak, nonatomic) IBOutlet UIView *viewVouchers;
@property (weak, nonatomic) IBOutlet UIView *viewNewsLetter;
@property (weak, nonatomic) IBOutlet UIView *viewFavourite;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (strong, nonatomic) IBOutlet UIButton *btnDrawer;
@property (strong, nonatomic) IBOutlet UIButton *btnNotification;
@property (nonatomic, strong) UIPopoverController *notifyAndLocationPopover;

- (IBAction)btnSettingClick:(id)sender;
- (IBAction)btnNewsletterClick:(id)sender;
- (IBAction)btnFavouriteClick:(id)sender;
- (IBAction)btnReviewClick:(id)sender;
- (IBAction)btnVoucherClick:(id)sender;
- (IBAction)btnBackClick:(id)sender;
- (IBAction)btnActionToClickDrawer:(id)sender;
- (IBAction)btnActionToClickNotification:(id)sender;
- (IBAction)btnSaveClick:(id)sender;
@end
