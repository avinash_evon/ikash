//
//  CategoryDetailsVC.h
//  iKash
//
//  Created by IndiaNIC on 03/12/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UILabel *lblNoRecordFound;

@property (strong, nonatomic) IBOutlet UITableView *tblCategoryDetails;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectAll;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) NSMutableArray *mutArrSubCatList;
@property (strong, nonatomic) NSString *strTitle;
@property (strong, nonatomic) NSString *strBranchId;
-(IBAction)btnDoneClick:(id)sender;
-(IBAction)btnBackClick:(id)sender;
- (IBAction)btnSelectAllAction:(id)sender;
@end
