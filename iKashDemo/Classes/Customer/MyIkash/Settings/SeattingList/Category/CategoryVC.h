//
//  CategoryVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate, UIPopoverControllerDelegate>{
    
    IBOutlet NSLayoutConstraint *clnViewBottomConstraint;
    
}

@property (nonatomic, strong) UIPopoverController *categoryDetailsPopover;
@property (strong, nonatomic) IBOutlet UIButton *btnPopOver;
@property(nonatomic,assign)IBOutlet UICollectionView *collectionViewCategory;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;

-(IBAction)btnNextClick:(id)sender;

@end
