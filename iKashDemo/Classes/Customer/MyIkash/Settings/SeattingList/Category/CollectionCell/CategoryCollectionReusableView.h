//
//  CategoryCollectionReusableView.h
//  iKash
//
//  Created by IndiaNIC on 12/05/16.
//  Copyright © 2016 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCollectionReusableView : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@end
