//
//  ChangePasswordVC.h
//  iKash
//
//  Created by indianic on 25/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordVC : UIViewController<UITextViewDelegate>{

    UIToolbar *toolBarTextView;
    
}

- (IBAction)backBtnClk:(UIButton *)sender;

- (IBAction)saveEmailBtnClk:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrllView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleChangePassword;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleNewPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleConfirmPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleChangeSecurity;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSelectQuestion;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleYourAnswer;

@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldSelectQuestion;

@property (weak, nonatomic) IBOutlet UITextView *txtViewAnswer;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewSelectQuestion;

@property (strong, nonatomic) IBOutlet UIButton *btnChangePass;
@property (strong, nonatomic) IBOutlet UIButton *btnChangeSecurity;

@property (weak, nonatomic) IBOutlet UITableView *tblQuestion;
@property (strong, nonatomic) IBOutlet UIView *viewtblBackground;


- (IBAction)cancelBtnClk:(UIButton *)sender;

- (IBAction)btnActionToChangePassClick:(id)sender;
- (IBAction)btnSelectQuestionClick:(id)sender;
- (IBAction)btnActionToChangeSecurityClick:(id)sender;
@end
