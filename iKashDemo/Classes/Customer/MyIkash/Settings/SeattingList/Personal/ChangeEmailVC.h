//
//  AddPromotionalCreditVC.h
//  iKashSupplier
//
//  Created by ind502 on 10/25/16.
//  Copyright © 2016 IndiaNIC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ContactDetailVC;

@interface ChangeEmailVC : UIViewController

- (IBAction)backBtnClk:(UIButton *)sender;

- (IBAction)saveEmailBtnClk:(UIButton *)sender;

- (IBAction)cancelBtnClk:(UIButton *)sender;



@property (strong, nonatomic) NSString * strEmailId;
@property (nonatomic, copy) void (^changeEmailSucessCB)(NSString *);
@property (nonatomic, copy) void (^backButtonClicked)(void);

@property (weak, nonatomic) IBOutlet UITextField *txtCurrentEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmNewEmail;

@end
