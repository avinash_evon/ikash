//
//  EditProfileDetailsVC.h
//  iKash
//
//  Created by AVINASH on 03/01/21.
//  Copyright © 2021 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditProfileDetailsVC : UIViewController


- (IBAction)maleBtnClk:(UIButton *)sender;

- (IBAction)femaleBtnClk:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UITextField *firstNameTxt;

@property (weak, nonatomic) IBOutlet UITextField *lastNameTxt;

@property (weak, nonatomic) IBOutlet UITextField *PhoneNumberTxt;

@property (weak, nonatomic) IBOutlet UITextField *DateOfBirthTxt;



- (IBAction)BackBtnAction:(UIButton *)sender;


- (IBAction)personalInfoSaveBtnClk:(UIButton *)sender;
- (IBAction)cancelBtnClk:(UIButton *)sender;


@end




NS_ASSUME_NONNULL_END
