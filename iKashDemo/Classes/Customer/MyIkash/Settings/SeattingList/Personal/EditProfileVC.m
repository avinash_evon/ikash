//
//  EditProfileVC.m
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "EditProfileVC.h"
#import "CategoryVC.h"
#import "SupplierVC.h"
#import "SecurityVC.h"
#import "ChangeEmailVC.h"

@interface EditProfileVC (){
    
    NSString *strGener;
    NSString *strCityId;
    NSString *strCountryId;
    NSDictionary *mutDicProfile;
    UIInterfaceOrientation orientationDevice;
    
    
    UIDatePicker *datePicker1;
    NSDate *date;
     UIToolbar *keyboardToolbar;
    
    BOOL editPersonalInfo;
    
    // city town list
    NSArray * mutArrCities;
    NSMutableArray * mutArrStrCities;
    
}

@end

@implementation EditProfileVC

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self datePicker];
    
    _txtViewAddress.delegate=self;
    
    mutArrStrCities = [[NSMutableArray alloc] init];
    
    _txtFieldCity.autoCompleteDelegate = self;
    _txtFieldCity.autoCompleteDataSource = self;
    _txtFieldCity.autoCompleteTableCellTextColor = kAppSupportedColorThemeColor;
    _txtFieldCity.autoCompleteTableBorderColor = kAppSupportedColorThemeColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    UINavigationBar* navigationbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, UIApplication.sharedApplication.statusBarFrame.size.height, self.view.frame.size.width, 50)];
//    UINavigationItem* navigationItem = [[UINavigationItem alloc] initWithTitle:@"Title"];
//    [navigationbar setItems:@[navigationItem]];
//    [self.view addSubview:navigationbar];
    
    dateFormat = [[NSDateFormatter alloc] init];
    
    if([[AppDelegate sharedInstance].strFlag isEqualToString:@"0"]){
        
        _btnNext.hidden=TRUE;// TRUE Avinash
        
        if (IS_IPAD) {
            scrllViewBottomConstraint.constant = 0;
        }else{
            scrllViewBottomConstraint.constant = 0;
        }
    }else{
        
        
         _btnNext.hidden=FALSE;
        _btnMale.selected = YES;
        _btnFemale.selected = NO;
        strGener=@"Male";
        
    }
    
    [self ProfileInfoWS];
    
    _txtFieldCountry.text=@"Netherlands";
    strCountryId=@"150";
    
    [self SetUpPage];
    
    orientationDevice = [UIApplication sharedApplication].statusBarOrientation;
    if(!IS_IPAD)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"EditData" object:nil];
}

// by Avinash

- (IBAction)BackBtnClk:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}




#pragma mark- Notification Methods

-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    orientationDevice=[UIApplication sharedApplication].statusBarOrientation;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=38;
            
        }];
    }
    NSLog(@"Hide");
}
- (void)keyboardWillShow:(NSNotification*)notification
{
    if(orientationDevice == UIInterfaceOrientationLandscapeLeft || orientationDevice == UIInterfaceOrientationLandscapeRight)
    {
        [UIView animateWithDuration:2.0 animations:^{
            [AppDelegate sharedInstance].WelCometopConstraint.constant=0;
            
        }];
    }else{
        [AppDelegate sharedInstance].WelCometopConstraint.constant=64;
    }
    
    NSLog(@"Show");
}

#pragma mark - Button Event Click Methods

- (IBAction)btnManageBranchClick:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    
    CategoryVC *objCategoryVC = [storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
    
    [self.navigationController pushViewController:objCategoryVC animated:YES];
}

- (IBAction)btnActionToolTipTemplete:(id)sender {
    
    NSString *message=@"I'm a popover popping over I'm a popover popping over I'm a popover popping over";
    
    [appDelegate btnActionToolTipTemplete:sender scroll:_scrllview   description:message];
    
}

- (IBAction)btnGetStartedClick:(id)sender {
    
   /* if (![_txtFieldFirstName.text isValidString]) {
        [_txtFieldFirstName becomeFirstResponder];
      //  [appDelegate showAlertView:[appDelegate getString:@"ENTER_FIRSTNAME"] Tag:@"0"];
       // return;
    }else if (![_txtFieldLastName.text isValidString]) {
        [_txtFieldLastName becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_LASTNAME"] Tag:@"0"];
        return;
    }else  if (![_txtFieldDOB.text isValidString]) {
        [_txtFieldDOB becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_DATEOFBIRTH"] Tag:@"0"];
        return;
    }*/
   /* else if ([_txtFieldPhoneNo.text length] <10) {
        [_txtFieldPhoneNo becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_TELEPHONE_10"] Tag:@"0"];
        return;
    }*/
   /* else*/
    
    
    
    
   /* if (![_txtFieldZipCode.text isValidString]) {
        [_txtFieldZipCode becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_ZIPCODE"] Tag:@"0"];
        return;
    }
    else if (_txtFieldZipCode.text.length!=7) {
        [_txtFieldZipCode becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"Please enter Zipcode in 'NNNN CC' Format"] Tag:@"0"];
        return;
    }
    else if (![_txtViewAddress.text isValidString]) {
        [_txtViewAddress becomeFirstResponder];
        [appDelegate showAlertView:[appDelegate getString:@"ENTER_ADDRESS"] Tag:@"0"];
        return;
    }*/
    
    [self.view endEditing:YES];
    if(strCityId.length>0)
    {
        [self PersonalInfoWS];
    }else{
        [appDelegate showAlertView:[appDelegate getString:@"Please enter valid zipcode"] Tag:@"0"];
    }
}



- (IBAction)btnMaleClick:(id)sender {
    
    _btnMale.selected = YES;
    _btnFemale.selected = NO;
    strGener=@"Male";
    
}

- (IBAction)btnFemaleClick:(id)sender {
    
    _btnMale.selected = NO;
    _btnFemale.selected = YES;
    strGener=@"Female";
    
}

-(IBAction)doneClicked:(id)sender{
    
    _toolBar.hidden = YES;
    _datePicker.hidden = YES;
    
}

-(IBAction)cancelClicked:(id)sender{
    
    [self keyboardWillHide:nil];
    _toolBar.hidden = YES;
    _datePicker.hidden = YES;
    
}

//- (IBAction)datePickerValueChanged:(id)sender {
//
//    [dateFormat setDateFormat:@"dd/M/yyyy"];
//    NSDate *dateTemp = [dateFormat dateFromString:[NSString stringWithFormat:@"%@",[dateFormat stringFromDate:_datePicker.date]]];
//    [dateFormat setDateFormat:@"dd/MM/yyyy"];
//    _txtFieldDOB.text =[dateFormat stringFromDate:dateTemp];
//
//}


- (IBAction)btnDoneClick:(id)sender {
    // Hide keyboard on tap event..
    
    [self keyboardWillHide:nil];
    
    if ([_txtFieldPhoneNo isFirstResponder]) {
        [_txtFieldPhoneNo resignFirstResponder];
    }
    if ([_txtViewAddress isFirstResponder]) {
        [_txtViewAddress resignFirstResponder];
    }
}

- (IBAction)btnChangeEmailClick:(id)sender{
    UIStoryboard *aStoryboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    ChangeEmailVC *objChangeEmailVC = [aStoryboard instantiateViewControllerWithIdentifier:@"ChangeEmailVC"];
    objChangeEmailVC.strEmailId =  [mutDicProfile objectForKey:@"c_email"];
    
    if (IS_IPAD) {
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objChangeEmailVC];
        navigationController.navigationBar.hidden = YES;
        self.changeEmailPopover = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        self.changeEmailPopover.delegate = self;
        self.changeEmailPopover.popoverContentSize = CGSizeMake(370.0, 400.0);
        self.view.alpha = 0.6;
        
        objChangeEmailVC.changeEmailSucessCB = ^(NSString *newEmail){
            _txtFieldEmail.text = newEmail;
            self.view.alpha = 1.0;
        };
        
        objChangeEmailVC.backButtonClicked = ^{
            self.view.alpha = 1.0;
        };
        
        CGRect aViewFrame = self.view.frame;
        
        [self.changeEmailPopover presentPopoverFromRect:CGRectMake((aViewFrame.size.width/2)-190, (aViewFrame.size.height/2)-200, 200, 400)
                                                 inView:self.view
                               permittedArrowDirections:0
                                               animated:YES];
    }else{
        [self.navigationController pushViewController:objChangeEmailVC animated:YES];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"EditData" object:nil];
    }
    
}

- (IBAction)editPersonalInfo:(id)sender {
}
// from edit personal screen
- (IBAction)cancel:(UIButton *)sender {
    
    
    NSLog(@"####################");
    
   // NSLog(@"first name %@",_txtFieldFirstName.text);
    
    
    _txtFieldFirstName.text = @"";
    _txtFieldLastName.text = @"";
    _txtFieldDOB.text = @"";
    _txtFieldPhoneNo.text = @"";
}

- (IBAction)back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(IBAction)BackAddress:(id)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
    
    
}

- (IBAction)cancelAddress:(id)sender {
    
    
    _txtFieldZipCode.text = @"";
    _txtFieldHouseNo.text = @"";
    _txtFieldExtention.text = @"";
    _txtStreetAddress.text = @"";
    _txtFieldCity.text = @"";
    _txtFieldCountry.text = @"";
}

- (IBAction)SaveAddress:(id)sender {
    
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                    
                                    _txtFieldZipCode.text,@"zip_code",
                                    
                                    strCityId,@"city_id",
                                    strCountryId,@"country_id",
                                    [self SendEmoji:_txtStreetAddress.text],@"street_address",
                                    [self SendEmoji:_txtFieldExtention.text] ,@"extension",
                                    _txtFieldHouseNo.text,@"house_no",
                                    nil];
      
      
      [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
          
          NSLog(@"responseData = %@", responseData);
          
          if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
              
              [USERDEFAULTS setObject:_txtFieldFirstName.text forKey:@"FirstName"];
              [USERDEFAULTS setObject:_txtFieldLastName.text forKey:@"LastName"];
              [USERDEFAULTS synchronize];
              
              
              [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
              
              if([[AppDelegate sharedInstance].strFlag isEqualToString:@"1"]){
                  [[NSNotificationCenter defaultCenter] postNotificationName:@"CategoryTab" object:nil];
              }
              
          }else{
              
              [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
              
          }
      } withFailureBlock:^(NSError *error) {
          NSLog(@"%@",error);
      }];
    
}

-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
    self.view.alpha = 1.0;
}

#pragma mark- UITextField

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    _toolBar.hidden = YES;
  //  _datePicker.hidden = YES;
    [textField resignFirstResponder];
    [self.view endEditing:YES];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    _toolBar.hidden = YES;
  //  _datePicker.hidden = YES;
    [self.view endEditing:YES];
    [textField resignFirstResponder];
    if(textField==_txtFieldZipCode){
        if(![_txtFieldZipCode.text isEqualToString:@""])
            strCityId=@"";
        [self getCity];
    }
    
    if (textField == _txtFieldCity){
        _ConstHeightViewCity.constant = 65;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
   // _datePicker.hidden = YES;
    _toolBar.hidden = YES;
    if (textField.tag == 101){
        if (IS_IPHONE) {
            
            [self keyboardWillShow:nil];
            
            _scrllview.contentOffset = CGPointMake(0, 180.5);
            
        }
        
      ///  _datePicker.hidden = NO;
        _toolBar.hidden = NO;
        [_datePicker setBackgroundColor:[UIColor whiteColor]];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        _datePicker.maximumDate = [NSDate date];
        if ([_txtFieldDOB.text isEqualToString:@""] || [_txtFieldDOB.text isEqualToString:@"0000-00-00"]) {
            [_datePicker setDate:[NSDate date]];
        }else{
            NSDate *date = [dateFormat dateFromString:_txtFieldDOB.text];
            [_datePicker setDate:date];
        }
        [self.view endEditing:YES];
        return NO;
    }
    
    if (textField == _txtFieldCity){
    _ConstHeightViewCity.constant = 150;
    }
    return YES;
}
// To make filter for NNNN CC format
NSMutableString *filteredPhoneStringFromStringWithFilter(NSString *string, NSString *filter)
{
    NSUInteger onOriginal = 0, onFilter = 0, onOutput = 0;
    char outputString[([filter length])];
    BOOL done = NO;
    
    while(onFilter < [filter length] && !done)
    {
        char filterChar = [filter characterAtIndex:onFilter];
        char originalChar = onOriginal >= string.length ? '\0' : [string characterAtIndex:onOriginal];
        switch (filterChar) {
            case '#':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isdigit(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            case '@':
                if(originalChar=='\0')
                {
                    // We have no more input numbers for the filter.  We're done.
                    done = YES;
                    break;
                }
                if(isalpha(originalChar))
                {
                    outputString[onOutput] = originalChar;
                    onOriginal++;
                    onFilter++;
                    onOutput++;
                }
                else
                {
                    onOriginal++;
                }
                break;
            default:
                // Any other character will automatically be inserted for the user as they type (spaces, - etc..) or deleted as they delete if there are more numbers to come.
                outputString[onOutput] = filterChar;
                onOutput++;
                onFilter++;
                if(originalChar == filterChar)
                    onOriginal++;
                break;
        }
    }
    outputString[onOutput] = '\0'; // Cap the output string
    return [[NSString stringWithUTF8String:outputString] mutableCopy];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if(textField == _txtFieldFirstName || textField == _txtFieldLastName)
    {
        
        if ([textField.text length] > 250) {
            NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            textField.text = [textField.text substringToIndex:changedString.length-1];
            return NO;
        }
        return YES;
    }
    
    else if( textField == _txtFieldHouseNo)
    {
        
        if (!string.length)
        {
            return YES;
        }
        
        if (![string isValidNumber]) {
            //textField.text = [textField.text substringToIndex:MAXLENGTH100-1];
            return NO;
        }
        
        
        if ([textField.text length] > 10) {
            NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            textField.text = [textField.text substringToIndex:changedString.length-1];
            return NO;
        }
        return YES;
    }
    
    else if(textField == _txtFieldPhoneNo)
    {
        
        NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (!string.length)
        {
            return YES;
        }
        
        if (![string isValidNumber]) {
            //textField.text = [textField.text substringToIndex:MAXLENGTH100-1];
            return NO;
        }
        
        if (changedString.length>10) {
            //textField.text = [textField.text substringToIndex:MAXLENGTH100-1];
            return NO;
        }
        
        
        
        return YES;
    }
    
    else if(textField == _txtFieldExtention)
    {
        
        
        
        if (!string.length)
        {
            return YES;
        }
        
        if (![string isValidAlfa]) {
            //textField.text = [textField.text substringToIndex:MAXLENGTH100-1];
            return NO;
        }
        if ([textField.text length] > 10) {
            NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            textField.text = [textField.text substringToIndex:changedString.length-1];
            return NO;
        }
        
        
        
        
        
        return YES;
    }
    
    else if(textField == _txtFieldZipCode)
    {
        NSString *filter = @"#### @@";
        
        if(!filter) return YES; // No filter provided, allow anything
        
        NSString *changedString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if(range.length == 1 && // Only do for single deletes
           string.length < range.length &&
           [[textField.text substringWithRange:range] rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound)
        {
            // Something was deleted.  Delete past the previous number
            NSInteger location = changedString.length-1;
            if(location > 0)
            {
                for(; location > 0; location--)
                {
                    if(isdigit([changedString characterAtIndex:location]))
                    {
                        break;
                    }
                }
                changedString = [changedString substringToIndex:location];
            }
        }
        textField.text = filteredPhoneStringFromStringWithFilter(changedString, filter);
        return NO;
    }else{
        return YES;
    }
    
}
-(NSString *)EmojiTest: (NSString *)strString{
    
    NSData *dataa = [strString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *valueEmoj = [[NSString alloc] initWithData:dataa encoding:NSNonLossyASCIIStringEncoding];
    
    return valueEmoj;
}
-(NSString *)SendEmoji: (NSString *)strString{
    
    NSData *data = [strString dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *valueEmoj = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    return valueEmoj;
}


#pragma mark - Webservice Methods
- (void)PersonalInfoWS{
    
    NSString *strDOB = nil;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate *dateTemp = [dateFormat dateFromString:[NSString stringWithFormat:@"%@",_txtFieldDOB.text]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    strDOB = [dateFormat stringFromDate:dateTemp];
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  strGener,@"gender",
                                //  [self SendEmoji:_txtFieldFirstName.text],@"first_name",
                                 
                                _txtFieldFirstName.text,@"first_name",
                                  
                                  [self SendEmoji:_txtFieldLastName.text],@"last_name",
                                    strDOB,@"dob",
                                  _txtFieldPhoneNo.text,@"phone_number",
                                //  _txtFieldFirstName.text,@"phone_number",
                               //    strDOB,@"dop",
                                  
           //     [self SendEmoji:_txtFieldPhoneNo.text],@"phone_number",
                                  
                                  _txtFieldZipCode.text,@"zip_code"];
                                  
                               /*   strCityId,@"city_id",
                                  strCountryId,@"country_id",
                                  [self SendEmoji:_txtStreetAddress.text],@"street_address",
                                  [self SendEmoji:_txtFieldExtention.text] ,@"extension",
                                  _txtFieldHouseNo.text,@"house_no",
                                  nil];*/
    
    
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_PersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            [USERDEFAULTS setObject:_txtFieldFirstName.text forKey:@"FirstName"];
            [USERDEFAULTS setObject:_txtFieldLastName.text forKey:@"LastName"];
            [USERDEFAULTS synchronize];
            
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
            if([[AppDelegate sharedInstance].strFlag isEqualToString:@"1"]){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CategoryTab" object:nil];
            }
            
        }else{
            
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
            
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)getCity{
    
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  _txtFieldZipCode.text,@"zipcode",
                                  
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityfromZipWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        NSLog(@"responseData = %@", responseData);
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            NSMutableArray *mutArrCityCountry=[[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"] ];
            
            _txtFieldCity.text=[[mutArrCityCountry objectAtIndex:0] objectForKey:@"city"];
            _txtFieldCountry.text=[[mutArrCityCountry objectAtIndex:0] objectForKey:@"country"];
            strCityId=[[mutArrCityCountry objectAtIndex:0] objectForKey:@"city_id_zip"];
            strCountryId=[[mutArrCityCountry objectAtIndex:0] objectForKey:@"country_id"];
            
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}


// getting details from server
- (void)ProfileInfoWS{
    
    NSMutableDictionary *aDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                  [USERDEFAULTS objectForKey:@"customer_id"],@"customer_id",
                                  nil];
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetPersonalInfoWS withParams:aDict showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        
        NSLog(@"responseData = %@", responseData);
        
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            
            mutDicProfile=[[NSDictionary alloc]initWithDictionary:[[responseData objectForKey:@"data"] objectAtIndex:0]];
            NSLog(@"%@",mutDicProfile);
            
            _txtFieldFirstName.text= [self EmojiTest:[mutDicProfile objectForKey:@"first_name"]];
            _txtFieldLastName.text=[self EmojiTest:[mutDicProfile objectForKey:@"last_name"]];
            
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *dateTemp = [dateFormat dateFromString:[NSString stringWithFormat:@"%@",[mutDicProfile objectForKey:@"date_of_birth"]]];
            [dateFormat setDateFormat:@"dd/MM/yyyy"];
            _txtFieldDOB.text =[dateFormat stringFromDate:dateTemp];
            
            _txtFieldPhoneNo.text= [self EmojiTest:[mutDicProfile objectForKey:@"phone_no"]];
            _txtFieldZipCode.text=[mutDicProfile objectForKey:@"zip_code"];
            _txtFieldCity.text=[mutDicProfile objectForKey:@"city"];
            _txtFieldCountry.text=[mutDicProfile objectForKey:@"country"];
            
            _txtStreetAddress.text=[self EmojiTest:[mutDicProfile objectForKey:@"address"]];
            _txtFieldEmail.text=[self EmojiTest:[mutDicProfile objectForKey:@"c_email"]];
            
            
            _txtFieldHouseNo.text=[self EmojiTest:[mutDicProfile objectForKey:@"house_number"]];
            _txtFieldExtention.text=[self EmojiTest:[mutDicProfile objectForKey:@"extension"]];
            strCityId=[mutDicProfile objectForKey:@"city_id"];
            strCountryId=[mutDicProfile objectForKey:@"country_id"];
            
            if([[mutDicProfile objectForKey:@"gender"] isEqualToString:@"Female"]){
                _btnMale.selected = NO;
                _btnFemale.selected = YES;
                strGener=@"Female";
            }else{
                _btnMale.selected = YES;
                _btnFemale.selected = NO;
                strGener=@"Male";
            }
            
            [USERDEFAULTS setObject:[mutDicProfile objectForKey:@"first_name"] forKey:@"FirstName"];
            [USERDEFAULTS synchronize];
            // [_tblSuppiler reloadData];
        }else{
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
#pragma mark SetUpPage

-(void)SetUpPage{
    
    [AppDelegate sharedInstance].strMyikashSaveButton=@"0";
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScanCompleted" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SaveButton" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btnGetStartedClick:) name:@"EditData" object:nil];
    self.txtViewAddress.layer.borderColor = [[appDelegate colorWithHexString:@"cdcbcc"] CGColor];
    self.txtViewAddress.layer.borderWidth = 0.5;
    self.txtViewAddress.layer.cornerRadius = 2.5f;
    
    _txtFieldEmail.hidden = [_strEditEmailShouldEnable isEqualToString:@"0"];
    _btnChangeEmail.hidden = [_strEditEmailShouldEnable isEqualToString:@"0"];
    _btnEmail.hidden = [_strEditEmailShouldEnable isEqualToString:@"0"];
    
    toolBarTextView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    
    NSMutableArray *items = [[NSMutableArray alloc] init];
    UIBarButtonItem *aBarBtnFlexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *aBarBtnDone = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(btnDoneClick:)];
    aBarBtnDone.tintColor = [UIColor blackColor];
    [items addObject:aBarBtnFlexibleItem];
    [items addObject:aBarBtnDone];
    [toolBarTextView setItems:items animated:NO];
    
    _txtFieldPhoneNo.inputAccessoryView = toolBarTextView;
    
    _txtViewAddress.inputAccessoryView = toolBarTextView;
    //_txtFieldZipCode.inputAccessoryView = toolBarTextView;
}

#pragma mark - auto completion mathods -

- (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField possibleCompletionsForString:(NSString *)string {
//    _scrllview.userInteractionEnabled = false;
    
    if(mutArrCities.count > 0){
        return mutArrStrCities;
    } else {
        [self GetCitiesWS];
    }
    return @[];
}

- (void) GetCitiesWS {
    [[Webservice sharedInstance] callWebserviceWithMethodName:customer_GetCityWS withParams:nil showConnectionError:YES forView:self.view withCompletionBlock:^(NSDictionary *responseData) {
        if ([[[responseData objectForKey:@"settings"] objectForKey:@"success"] integerValue] == 1) {
            mutArrCities = [[NSMutableArray alloc]initWithArray:[responseData objectForKey:@"data"]];
            [mutArrStrCities removeAllObjects];
            [mutArrCities enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [mutArrStrCities addObject:[obj objectForKey:@"city_name"]];
            }];
        }else{
            NSLog(@"%@",[[responseData objectForKey:@"settings"] objectForKey:@"message"]);
            [appDelegate showAlertView:[[responseData objectForKey:@"settings"] objectForKey:@"message"] Tag:[[responseData objectForKey:@"settings"] objectForKey:@"success"]];
        }
    } withFailureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField didSelectAutoCompleteString:(NSString *)selectedString withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSPredicate* aPre = [NSPredicate predicateWithFormat:@"city_name = %@",_txtFieldCity.text];
    
    NSArray* aArr = [mutArrCities filteredArrayUsingPredicate:aPre];
    strCityId = [[aArr firstObject]objectForKey:@"city_id"];
}

# pragma mark - address auto complete -


-(void)datePicker
{
    if (keyboardToolbar == nil) {
           keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
           [keyboardToolbar setBarStyle:UIBarStyleBlackTranslucent];

           UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

           UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(closeKeyboard)];

           [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
       }

       _txtFieldDOB.inputAccessoryView = keyboardToolbar;

       datePicker1 = [[UIDatePicker alloc] init];
       datePicker1.datePickerMode = UIDatePickerModeDate;
       [datePicker1 addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];

      _txtFieldDOB.inputView = datePicker1;
}

- (void)datePickerValueChanged:(id)sender{

date = datePicker1.date;

NSDateFormatter *df = [[NSDateFormatter alloc] init];
[df setDateFormat:@"dd/MM/YYYY"];


    [_txtFieldDOB setText:[df stringFromDate:date]];
}

-(void)closeKeyboard
{
    [self.view endEditing:TRUE];
}

@end
