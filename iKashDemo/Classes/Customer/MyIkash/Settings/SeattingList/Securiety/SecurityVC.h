//
//  SecurityVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecurityVC : UIViewController{
   UIToolbar *toolBarTextView;
}

@property (weak, nonatomic) IBOutlet UIButton *selectQuestionOutlet;


- (IBAction)backBtnClk:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrllView;

@property (strong, nonatomic) IBOutlet UILabel *lblSelectSecurity;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewSelectQuestion;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldSelectQuestion;

@property (strong, nonatomic) IBOutlet UILabel *lblYourAnswer;
@property (strong, nonatomic) IBOutlet UITextView *txtViewYourAnswer;

@property (strong, nonatomic) IBOutlet UITableView *tblQuestion;
@property (strong, nonatomic) IBOutlet UIView *viewtblBackground;

- (IBAction)btnSelectQuestionClick:(id)sender;
- (IBAction)btnGetStartedClick:(id)sender;

- (IBAction)changePwnBtnClk:(UIButton *)sender;

@end
