//
//  UserVoucherListVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXStarRatingView.h"

@interface UserVoucherListVC : UIViewController<UIPopoverControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnPopupView;
@property (strong, nonatomic) IBOutlet UILabel *lblNoRecordFound;
@property (strong, nonatomic) IBOutlet UICollectionView *userVoucherClnView;
@property (nonatomic, strong) UIPopoverController *userVoucherPopover;
@property (nonatomic, strong) UIPopoverController *campaignDetailsPopover;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;
@end
