//
//  NotificationVC.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintHeight, *leftContraintBottom, *titleContraintBottom, *rightContraintBottom;

@property (strong, nonatomic) IBOutlet UITableView *tblViewNotification;
@property (strong, nonatomic) IBOutlet UILabel *lblNoRecordFound;
@property (strong, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;

- (IBAction)btnActionToBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewNoInternet;
@property (weak, nonatomic) IBOutlet UILabel *lblNoInternet;

@end
