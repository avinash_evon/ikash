//
//  PopOverAlertVC.m
//  iKash
//
//  Created by indianic on 26/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "PopOverAlertVC.h"
#import "LoginVC.h"
#import "RegisterVC.h"

@interface PopOverAlertVC ()

@end

@implementation PopOverAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_innerPopUpView.layer setCornerRadius:2.5f];
    [_innerPopUpView.layer setMasksToBounds:YES];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLoginClick:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    __block UINavigationController *aNv = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserLoginDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[AppDelegate sharedInstance].window.rootViewController isKindOfClass:[MFSideMenuContainerViewController class]]) {
        LoginVC *objLoginVC = [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
        aNv = [[UINavigationController alloc] initWithRootViewController:objLoginVC];
        [AppDelegate sharedInstance].window.rootViewController = aNv;
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [AppDelegate sharedInstance].window.rootViewController = self.navigationController;
    }
}

- (IBAction)btnRegisterClick:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:MainStoryBoard bundle:nil];
    __block UINavigationController *aNv = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserLoginDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
   
    if ([[AppDelegate sharedInstance].window.rootViewController isKindOfClass:[MFSideMenuContainerViewController class]]) {
        RegisterVC *objRegisterVC = [storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
        aNv = [[UINavigationController alloc] initWithRootViewController:objRegisterVC];
        [AppDelegate sharedInstance].window.rootViewController = aNv;
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
        [AppDelegate sharedInstance].window.rootViewController = self.navigationController;
    }
}

- (IBAction)btnCancelClick:(id)sender {
    [self.view removeFromSuperview];
}

@end
