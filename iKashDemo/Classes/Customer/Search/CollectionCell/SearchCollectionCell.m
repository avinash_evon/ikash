//
//  SearchCollectionCell.m
//  iKash
//
//  Created by indianic on 24/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import "SearchCollectionCell.h"

@implementation SearchCollectionCell
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self drowShadow];
        self.clipsToBounds = NO;
    }
    return self;
}

-(void)drowShadow{
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowOffset = CGSizeMake(1, 1);
    self.layer.shadowRadius = 1;
}

@end
