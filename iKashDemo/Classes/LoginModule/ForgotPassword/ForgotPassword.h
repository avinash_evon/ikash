//
//  ForgotPassword.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassword : UIViewController{
    IBOutlet NSLayoutConstraint *imgViewLogoTopConstraint;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondDescription;
@property (weak, nonatomic) IBOutlet UIView *ViewText;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSendmail;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginClick;

- (IBAction)btnSendMailClick:(id)sender;
- (IBAction)btnLoginClick:(id)sender;

@end
