//
//  ForgotPassword.h
//  iKashDemo
//
//  Created by indianic on 31/10/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionVC : UIViewController{
    IBOutlet NSLayoutConstraint *imgViewLogoTopConstraint;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgViewBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblQuestion;
@property (weak, nonatomic) IBOutlet UIView *ViewText;
@property (weak, nonatomic) IBOutlet UITextField *txtAwnser;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnSubmitClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotClick;
- (IBAction)btnForgotClick:(id)sender;
@property (strong, nonatomic)NSMutableArray *mutArrQuestionAnswer;
@end
