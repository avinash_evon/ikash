//
//  NoSwipeSegmentedControl.h
//  iKashDemo
//
//  Created by sameer on 19/06/20.
//  Copyright © 2020 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoSwipeSegmentedControl : UISegmentedControl

@end

NS_ASSUME_NONNULL_END
