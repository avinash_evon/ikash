//
//  Constant.h
//

/*-------------------- Navigation Lock Button ------------------ */



// Web Services


//#define WSURL @"http://php54.indianic.com/ikash_webservice/webservice/" // IndiaNIC Server
//#define WSURL @"http://ec2-54-93-78-175.eu-central-1.compute.amazonaws.com/webservice/webservice/" // android


//http://ec2-54-93-78-175.eu-central-1.compute.amazonaws.com/ shubham

//http://ec2-18-196-106-217.eu-central-1.compute.amazonaws.com/webservice/webservice/

#if DEV_SERVER
  #define WSURL @"http://ec2-18-196-106-217.eu-central-1.compute.amazonaws.com/webservice/webservice/" // evon dev

//@"http://ec2-54-93-78-175.eu-central-1.compute.amazonaws.com/"
  #define SUPPLIER_APP_IDENTIFIER @"iKashSupplierDev://"
#else
//  #define WSURL @"http://php54.indianic.com/ikash_webservice/webservice/" // IndiaNIC Server
  #define WSURL @"http://ec2-54-93-78-175.eu-central-1.compute.amazonaws.com/webservice/webservice/"  // android
  #define SUPPLIER_APP_IDENTIFIER @"iKashSupplier://"
#endif

#define APPTITLE                            @"iKash"
#define get_background_imageWS              @"get_background_image"
#define customer_loginWS                    @"customer_login"
#define customer_FB_loginWS                 @"check_facebook_id"
#define customer_Registration_loginWS       @"customer_registration"
#define customer_ForgotPassword_QuestionWS  @"customer_get_forgot_ques"
#define customer_ForgotPassword_AnswerWS    @"customer_check_answer"
#define customer_PersonalInfoWS             @"add_customer_personal_info"
#define customer_GetCityfromZipWS           @"get_city_id_from_zipcode"
#define customer_GetPersonalInfoWS          @"get_customer_personal_details"

#define customer_HomeChangePasswordWS       @"change_forgot_pwd"
#define customer_ChangePasswordWS           @"customer_change_password"
#define customer_FavSupplierWS              @"get_customer_fav_suppliers"
#define customer_PramotionCodeWS            @"check_customer_promotion_code"
#define customer_FavCategoryWS              @"get_customer_fav_branches"
#define customer_SecurityQuestionWS         @"get_customer_security_question"
#define customer_UnSubscribeWS              @"unsubscribe_customer"
#define customer_GetCategoryWS              @"get_categories"
#define customer_GetCampaignWS              @"home_screen_customer_final_revised_mobile" //@"get_campaigns_sub_cat_phone"
#define customer_SearchCampaignWS           @"home_screen_full_text"
#define customer_UpdatePushWS               @"update_push"
#define customer_GetCityWS                  @"get_city"
#define customer_GetCityLocationWS          @"get_city_id_from_latlong"
#define customer_GetNearCityWS              @"get_nearby_cities"

#define customer_GetCampaignHomeWS          @"home_screen_customer_final_revised_mobile" //@"home_customer_final_mobile"
#define customer_GetCampaignSearchWS        @"home_screen_customer_final"
#define customer_GetSupplierWS              @"get_supplier_with_logo"
#define customer_GetCampaignDetailsWS       @"campaign_detail"
#define customer_GetSupplierDetailsWS       @"campaign_supplier_details"
#define customer_download_voucher_firstWS   @"download_voucher_first"
#define customer_DownaloadVoucherWS         @"customer_download_voucher"
#define customer_IncreaseViewCountWS        @"view_campaign"
#define customer_RedeemVoucherWS            @"redeem_voucher_customer"

#define customer_GetFavouriteCampaignWS     @"favourite_campaign_customer"
#define customer_UnFavouriteCampaignWS      @"unfavourite_campaign"
#define customer_FavouriteCampaignWS        @"add_to_favourite_campaign"

#define customer_GetReviewRatingWS          @"customer_review_rating"
#define customer_DeleteMyReviewWS           @"delete_review"
#define customer_GetCampaignReviewWS        @"customer_campaign_review"
#define customer_AddReviewWS                @"submit_review"
#define customer_GetdownalodedVoucherWS     @"customer_downloaded_voucher"
#define customer_deleteVoucherWS            @"delete_downloaded_voucher"
#define customer_NotificationWS             @"get_notifications"

#define customer_changeEmailWS              @"customer_change_email"
#define customer_resendEmailWS              @"resend_verification_email_customer"



#define RGB(r, g, b)                        [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define HEXCOLOR(c)                         [UIColor colorWithRed:((c>>16)&0xFF)/255.0 green:((c>>8)&0xFF)/255.0 blue:(c&0xFF)/255.0 alpha:1.0];

// Storyboard
#define MainStoryBoard                      ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"Main_iPad" : @"Main")
#define ThirdStoryBoard                      ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? @"Main_iPad" : @"Main_Third")

#define IS_IPAD                             (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE                           (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_STANDARD_IPHONE_6                (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_STANDARD_IPHONE_6_PLUS           (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)

#define tileCellXibNibName                  IS_IPAD ? @"TileViewIPadCell" : @"TileViewCell"
#define tileCellReuseIdentifier             @"CampaignCollectionCell2"


#define ANIMATION_DURATION 0.5

#define IS_IPHONE_4 [[UIScreen mainScreen]bounds].size.height == 480
#define IS_IPHONE_5 [[UIScreen mainScreen]bounds].size.height == 568
#define IS_IPHONE_6 [[UIScreen mainScreen]bounds].size.height == 667
#define IS_IPHONE_6_PLUS [[UIScreen mainScreen]bounds].size.height == 736

#define USERDEFAULTS                                     [NSUserDefaults standardUserDefaults]

// Font Name
#define FontRobotoRegular                     @"Roboto-Regular"
#define FontRobotoMedium                      @"Roboto-Medium"
#define FontRobotoLight                       @"Roboto-Light"

//UserDefault keys
#define kRememberEmail                      @"rememberEmail"
#define kRememberPassword                   @"rememberPassword"


// Font Size
#define kAppSupportedFontSize20     10

// Color Code
#define kAppSupportedColorThemeColor            [UIColor colorWithHexString:@"#FE7935"]
#define kAppSupportedColorNormalColor           [UIColor colorWithHexString:@"#666666"]
#define kAppSupportedColorLightColor            [UIColor colorWithHexString:@"#333333"]
#define kAppSupportedColorDisableColor          [UIColor colorWithHexString:@"#cccccc"]
