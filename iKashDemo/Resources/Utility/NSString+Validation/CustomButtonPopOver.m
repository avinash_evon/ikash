//
//  MyButton.m
//  iKash
//
//  Created by indianic on 02/02/16.
//  Copyright (c) 2016 indianic. All rights reserved.
//

#import "CustomButtonPopOver.h"

@class AppDelegate;

//IB_DESIGNABLE

@implementation CustomButtonPopOver

-(void)setTitle:(NSString *)title forState:(UIControlState)state{
    super.titleLabel.text = [self getLanguageText:title];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    [super setTitle:[self getLanguageText:super.titleLabel.text] forState:UIControlStateNormal];
    //    [super setTitleColor:[appDelegate colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    //    [super setBackgroundColor:[appDelegate colorWithHexString:@"fe7935"] ];
    //    [super.titleLabel setFont:[UIFont fontWithName:FontRobotoMedium size:13]];
}
//Star for mandatory fields
-(NSString*)getLanguageText:(NSString*)str{
    //Remove star then get language text and add star again
    if ([str containsString:@" *"]){
        NSString *aStrWithOutStar = [str stringByReplacingOccurrencesOfString:@" *" withString:@""];
        return [NSString stringWithFormat:@"%@ *",[appDelegate getString:aStrWithOutStar]];
    }else{
        return [appDelegate getString:str];
    }
}

- (void) setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
}

- (CGFloat) cornerRadius {
    return self.layer.cornerRadius;
}

- (void) setBorderColor:(UIColor *)borderColor {
    self.layer.borderColor = borderColor.CGColor;
}

- (UIColor *) borderColor {
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void) setBorderWidth:(CGFloat)borderWidth {
    self.layer.borderWidth = borderWidth;
}

- (CGFloat) borderWidth {
    return self.layer.borderWidth;
}

-(CGRect)imageRectForContentRect:(CGRect)contentRect {
    CGRect imageFrame = [super imageRectForContentRect:contentRect];
    CGFloat imageWidth = imageFrame.size.width;
    CGRect titleRect = CGRectZero;
    titleRect.size = [[self titleForState:self.state] sizeWithAttributes:@{NSFontAttributeName: self.titleLabel.font}];
    switch(self.contentHorizontalAlignment) {
        case UIControlContentHorizontalAlignmentLeft:
            titleRect.origin.x = self.titleEdgeInsets.left - self.titleEdgeInsets.right;
            break;
        case UIControlContentHorizontalAlignmentRight:
            titleRect.origin.x = (self.frame.size.width - (titleRect.size.width + imageWidth)) - self.titleEdgeInsets.left + self.titleEdgeInsets.right;
            break;
        default:
            titleRect.origin.x = (self.frame.size.width - (titleRect.size.width + imageWidth)) / 2.0 + self.titleEdgeInsets.left - self.titleEdgeInsets.right;
    }
    
    imageFrame.origin.x = titleRect.origin.x + titleRect.size.width - self.imageEdgeInsets.right + self.imageEdgeInsets.left;
    return imageFrame;
}

-(CGRect)titleRectForContentRect:(CGRect)contentRect {
    CGFloat imageWidth = [self imageForState:self.state].size.width;
    CGRect titleFrame = [super titleRectForContentRect:contentRect];
    switch(self.contentHorizontalAlignment) {
        case UIControlContentHorizontalAlignmentLeft:
            titleFrame.origin.x = self.titleEdgeInsets.left - self.titleEdgeInsets.right;
            break;
        case UIControlContentHorizontalAlignmentRight:
            titleFrame.origin.x = (self.frame.size.width - (titleFrame.size.width + imageWidth)) - self.titleEdgeInsets.left + self.titleEdgeInsets.right;
            break;
        default:
            titleFrame.origin.x = (self.frame.size.width - (titleFrame.size.width + imageWidth)) / 2.0 + self.titleEdgeInsets.left - self.titleEdgeInsets.right;
    }
    return titleFrame;
}

-(void)sizeToFitWidth {
    CGSize size = [self sizeThatFits:CGSizeMake(CGFLOAT_MAX, self.frame.size.height)];
    CGRect frame = self.frame;
    frame.size.width = size.width;
    self.frame = frame;
}

@end
