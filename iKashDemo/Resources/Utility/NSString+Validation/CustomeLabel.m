//
//  VFSLabel.m
//  VFS
//
//  Created by indianic on 12/16/14.
//  Copyright (c) 2014 indianic. All rights reserved.
//

#import "CustomeLabel.h"

@implementation CustomeLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setText:(NSString *)text
{
    super.text = [appDelegate getString:text];
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    //Star for mandatory fields 
    //Remove star then get language text and add star again
    if ([super.text containsString:@" *"]){
        NSString *aStrWithOutStar = [super.text stringByReplacingOccurrencesOfString:@" *" withString:@""];
        super.text = [NSString stringWithFormat:@"%@ *",[appDelegate getString:aStrWithOutStar]];
    }else{
        super.text = [appDelegate getString:super.text];
    }
}

@end
