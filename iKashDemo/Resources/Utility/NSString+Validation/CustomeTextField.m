//
//  VFSTextField.m
//  VFS
//
//  Created by indianic on 12/16/14.
//  Copyright (c) 2014 indianic. All rights reserved.
//

#import "CustomeTextField.h"

IB_DESIGNABLE

@implementation CustomeTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setText:(NSString *)text
{
    super.text = [self getLanguageText:text];
}
-(void)awakeFromNib
{
    [super awakeFromNib];
    super.text = [self getLanguageText:super.text];
    super.placeholder = [self getLanguageText:super.placeholder];
    
//    [self setValue:[appDelegate colorWithHexString:@"666666"]
//                    forKeyPath:@"_placeholderLabel.textColor"];
//    super.font = [UIFont fontWithName:FontRobotoRegular size:13.0];
//    super.textColor = [appDelegate colorWithHexString:@"666666"];
    
}

//Star for mandatory fields
-(NSString*)getLanguageText:(NSString*)str{
    //Remove star then get language text and add star again
    if ([str containsString:@" *"]){
        NSString *aStrWithOutStar = [str stringByReplacingOccurrencesOfString:@" *" withString:@""];
        return [NSString stringWithFormat:@"%@ *",[appDelegate getString:aStrWithOutStar]];
    }else{
        return [appDelegate getString:str];
    }
}

-(void)setPlaceholder:(NSString *)placeholder
{
  super.text = [appDelegate getString:placeholder];
}

-(void)setBorderColor:(UIColor *)borderColor
{
    self.layer.borderColor=borderColor.CGColor;
    
}

-(UIColor *)borderColor
{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth
{
    self.layer.borderWidth=borderWidth;
}

-(CGFloat)borderWidth
{
    return self.layer.borderWidth;
}

-(void)setCornerRadius:(CGFloat)cornerRadius
{
    self.layer.cornerRadius=cornerRadius;
}

-(CGFloat)cornerRadius
{
    return self.layer.cornerRadius;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect inset = CGRectMake(bounds.origin.x + _padding, bounds.origin.y, bounds.size.width - _padding, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect inset = CGRectMake(bounds.origin.x + _padding, bounds.origin.y, bounds.size.width - _padding, bounds.size.height);
    return inset;
}

@end
