//
//  VFSTextField.h
//  VFS
//
//  Created by indianic on 12/16/14.
//  Copyright (c) 2014 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomeTextFieldPaste : UITextField

@property (nonatomic ,assign) IBInspectable CGFloat borderWidth;
@property (nonatomic ,assign) IBInspectable UIColor *borderColor;
@property (nonatomic ,assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat padding;

@end
