//
//  customAutoCompleteTextField.h
//  iKash
//
//  Created by indianic on 06/10/17.
//  Copyright © 2017 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPAutoCompleteTextField.h"
#import "MLPAutoCompleteTextFieldDelegate.h"
#import "MLPAutoCompleteTextFieldDataSource.h"

@interface customAutoCompleteTextField : MLPAutoCompleteTextField

@property (nonatomic ,assign) IBInspectable CGFloat borderWidth;
@property (nonatomic ,assign) IBInspectable UIColor *borderColor;
@property (nonatomic ,assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat padding;

@end
