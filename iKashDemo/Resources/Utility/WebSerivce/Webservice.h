//
//  Webservice.h
//  Nobby
//
//  Created by ind563 on 7/21/14.
//  Copyright (c) 2014 Indianic. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Webservice : NSObject
{
    NSDictionary *dictDownload;
}
+(Webservice *)sharedInstance;

@property(nonatomic,copy)NSDictionary *dictMetaData;

-(void)callWebserviceWithMethodName:(NSString *)aStrURL withParams:(NSMutableDictionary *)aDict showConnectionError:(BOOL)aBoolVal forView:(UIView*)aVWObj withCompletionBlock:(void(^)(NSDictionary * responseData))completionBlock withFailureBlock:(void(^)(NSError * error))failureBlock ;

-(void)callWebserviceWithMethodName:(NSString *)aStrServiceName withParams:(NSMutableDictionary *)aDict showConnectionError:(BOOL)aBoolVal showLoader : (BOOL) flagLoader forView:(UIView*)aVWObj withCompletionBlock:(void(^)(NSDictionary * responseData))completionBlock withFailureBlock:(void(^)(NSError * error))failureBlock;
@end
