//
//  SuppilerCell.h
//  iKashDemo
//
//  Created by IndiaNIC on 17/11/15.
//  Copyright © 2015 indianic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuppilerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnCheck;

@property (strong, nonatomic) IBOutlet UILabel *lblSupplierName;

@property (nonatomic,weak) IBOutlet UIImageView *imgViewSupplierImage;

@end
